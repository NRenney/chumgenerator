#ChumGenerator
###This bad boy pumps out the ChumBucket cards

---
##Pre-Setup:
Install git from here:
[Install me](https://git-scm.com/download/mac "Meep Meep")

---
##Setup:
```
cd Documents/
git clone https://NRenney@bitbucket.org/NRenney/chumgenerator.git
```
---
##Update with:
```
cd Documents/
git pull
```
---
##Usage:

* Open up terminal
* run the following commands
```
cd Documents/ChumGenerator/
python chumMaker.py
```
---
#### You will need the following in the ChumGenerator folder
> *nb. It will fail if any of these are missing*

* Animals.csv
* Spells.csv
* AnimalTemplate_Front.png
* SpellTemplate_Front.png
* An Animals Folder
* A Spells Folder
* those two fonts too (I might make it so that you can change these - I mean you can anyway if you just change the names)

### Spells.csv should follow the following form
Spell Type | Spell Name |	Spell Words |	Spell Function |Quantity in Deck | Description in Rules
--- | --- |--- |--- |--- |--- |
Turn	|Single Drink	| Drinkus! |Target takes one drink.|	6	| Force the target to take a drink from whatever beverage they're sucking on.

---
### Animals.csv should follow the following form
You're now a |
---|
Chicken choking down a worm