from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import textwrap
import csv

count = 0
AniDesc_pos = (52,210)

csvSourceAnimals_fn = "Animals.csv"
csvSourceSpells_fn = "Spells.csv"
cardFrontAnimals_fn = "AnimalTemplate_Front.png"
cardFrontSpells_fn = "SpellTemplate_Front.png"

#   ___        _                 _
#  / _ \      (_)               | |
# / /_\ \_ __  _ _ __ ___   __ _| |___
# |  _  | '_ \| | '_ ` _ \ / _` | / __|
# | | | | | | | | | | | | | (_| | \__ \
# \_| |_/_| |_|_|_| |_| |_|\__,_|_|___/
with open(csvSourceAnimals_fn) as chumfile:
    daRealCSV = csv.reader(chumfile, delimiter=',')
    for row in daRealCSV:
        fontsize = 60
        img_fraction = 0.80
        img = Image.open(cardFrontAnimals_fn)
        draw = ImageDraw.Draw(img)
        textLines = textwrap.wrap(row[0], width=20)
        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE

        font = ImageFont.truetype("Carton_Six.ttf", fontsize)
        width, height = font.getsize(row[0])
        while font.getsize(textLines[0])[0] < img_fraction*img.size[0]:
            # iterate until the text size is just larger than the criteria
            fontsize += 1
            font = ImageFont.truetype("Carton_Six.ttf", fontsize)

        # optionally de-increment to be sure it is less than criteria
        fontsize -= 1
        font = ImageFont.truetype("Carton_Six.ttf", fontsize)
        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE

        for line in textLines:
            width = font.getsize(line)[0]
            draw.text(((img.width - width)/2,AniDesc_pos[1]+height), line,(255,255,255), font=font)
            height += font.getsize(line)[1]

        # draw.multiline_text(AniDesc_pos,text,(255,255,255),font=font,align="center")
        img.save('Animals/animal'+str(count) + '.png')
        count +=1
#  _____            _ _
# /  ___|          | | |
# \ `--. _ __   ___| | |___
#  `--. \ '_ \ / _ \ | / __|
# /\__/ / |_) |  __/ | \__ \
# \____/| .__/ \___|_|_|___/
#       | |
#       |_|
count = 0

# Only the Y value is actually used when they're being centred tbh
SpTy_pos = (52,80)
SpName_pos = (52,230)
SpWord_pos = (52,485)
SpFunc_pos = (52,822)

with open(csvSourceSpells_fn) as chumfile:
    daRealCSV = csv.reader(chumfile, delimiter=',')
    for row in daRealCSV:
        fontsize = 40
        img_fraction = 0.80
        img = Image.open(cardFrontSpells_fn)
        draw = ImageDraw.Draw(img)

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Spell Type
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE
        textLines = textwrap.wrap(row[0], width=20)#Spell Type
        font = ImageFont.truetype("daisywhl.otf", fontsize)
        width, height = font.getsize(row[0])

        # ++++++++++++++++++++++++++++++++++++++WRAP AND CENTRE JUSTIFY

        for line in textLines:
            width = font.getsize(line)[0]
            draw.text(((img.width - width)/2,SpTy_pos[1]+height), line,(255,255,255), font=font)
            height += font.getsize(line)[1]

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Spell Name
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE
        textLines = textwrap.wrap(row[1], width=20)#spell name
        font = ImageFont.truetype("daisywhl.otf", fontsize)
        width, height = font.getsize(row[1])

        # ++++++++++++++++++++++++++++++++++++++WRAP AND CENTRE JUSTIFY

        for line in textLines:
            width = font.getsize(line)[0]
            draw.text(((img.width - width)/2,SpName_pos[1]+height), line,(255,255,255), font=font)
            height += font.getsize(line)[1]
                # ++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Spell Words
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        fontsize = 60
        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE
        textLines = textwrap.wrap(row[2], width=20)#spell Words
        font = ImageFont.truetype("Carton_Six.ttf", fontsize)
        width, height = font.getsize(row[2])

        # ++++++++++++++++++++++++++++++++++++++WRAP AND CENTRE JUSTIFY

        for line in textLines:
            width = font.getsize(line)[0]
            draw.text(((img.width - width)/2,SpWord_pos[1]+height), line,(255,255,255), font=font)
            height += font.getsize(line)[1]
        # ++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Spell Function
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        fontsize = 40
        # ++++++++++++++++++++++++++++++++++++++SCALE FONT SIZE
        textLines = textwrap.wrap(row[3], width=20)#spell Function
        font = ImageFont.truetype("daisywhl.otf", fontsize)
        width, height = font.getsize(row[3])

        # ++++++++++++++++++++++++++++++++++++++WRAP AND CENTRE JUSTIFY

        for line in textLines:
            width = font.getsize(line)[0]
            draw.text(((img.width - width)/2,SpFunc_pos[1]+height), line,(255,255,255), font=font)
            height += font.getsize(line)[1]
        # ++++++++++++++++++++++++++++++++++++++
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Save Em
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        # draw.multiline_text(AniDesc_pos,text,(255,255,255),font=font,align="center")
        img.save('Spells/spell'+str(count) + '.png')
        count +=1
# draw.text((x, y),"Sample Text",(r,g,b))
